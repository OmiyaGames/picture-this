# Picture This!
### An Interpretative Art Exercise

## Objective

*Picture This! An Interpretative Art Exercise* is a customizable party game that can be played with as many even-numbered players as the room can fit in!  2 players must team-up to create a beautiful artwork only one of the player, the client, knows.  The client, however, can only use body gestures to communicate to their partner, the artist, what to draw.  Don't take too long, though; the team has a critical deadline to meet!

## Requirements

* Even number of players (minimum: 2). Each pair of players will act as a team.
* A deck of diagrams (can be printed from diagrams.pdf).  See also the section, **Create A Custom Diagram**.
* 1 pen and 1 sheet of paper for each team.
* A timer with an audible alarm (e.g. a smartphone app).
* A flat surface is recommended.

## How to Play

Each game is split between 3 phases: planning, drawing, and judging phase.

### Planning Phase

1. All the players should agree on the amount of time they should set the timer to during the **Drawing Phase**.  The recommended times are between 5 to 15 minutes.  *Do not start the timer yet!*
2. For each team, designate one player as the client, and the other as the artist.
3. Each client will draw a random card from the deck of diagrams.
	* This card cannot be revealed to any artist until the **Judging Phase**.
	* The client must hold onto the card until the **Judging Phase**.
	* So long as the card remains hidden from all the artists, the client may check it for reference at any time during the game.
	* *Alternative rule:* instead of each individual client getting their own card, only one card is drawn, and all the clients must memorize the diagram before shuffling the card back into the deck.
4. The client may spend any time planning on how to convey their card's diagram amongst themselves--see **Gestures** section for context--before moving into the **Drawing Phase** below.
5. The artists, however, may not eavesdrop on what the clients are discussing, let alone listen or join in the conversation.

### Drawing Phase

1. Each artist prepares their own sheet of paper.
	* Make sure all the artists are informed that the artwork the clients have in mind can be drawn without lifting their pen from the paper.
2. The client will let their artist know where they should place their pen's tip as the starting point by pointing at their paper.
3. When all the artists are ready, the clients needs to stand up in front of their artist, and face their direction.
	* Make sure each client is provided enough space to swing their arms freely without hitting anyone while communicating to their artist.
4. When all the clients are ready, start the timer.
5. Each client must use only the body gesture language described under the section, **Gestures**, to instruct their artist what to draw.
	* With exactly 1 exception--described in step 6 below--the client may not speak while instructing their artist.
6. The client can say either "close" or "done" to end their drawing session if the following conditions are met:
	* If the last step in completing the artwork involves drawing a straight line from where the artist's pen is at, back to the starting point, the client can say, "close" to indicate the artist only needs to draw one more line, effectively finishing the artwork and ending their drawing session.
	* Otherwise, the client can say, "done" whenever they are satisfied with the artist's work to end their drawing session early.
	* Finishing early does not affect the deadline in any way; the other teams may continue as normal.
7. Any time during the **Drawing Phase**, the artist must ask their client exactly 3 yes-or-no questions--no more, no less--that the client can answer.  Failure to do so prevents the team from getting extra points during the **Judging Phase**.  The client must respond to each of them by nodding (yes) or shaking their head (no).
	* Artists are not allowed to ask their client what the card's diagram is, or anything hinting what the diagram could be.
		* Not allowed: "is the diagram a heart?"
		* Not allowed: "is the diagram basic geometric shape?"
	* Otherwise, artists are allowed to ask their client any yes-or-no questions.
		* Allowed: "do you want me to curve this line more downwards?"
		* Allowed: "is my drawing close to what you're envisioning?"
		* Allowed: "are you feeling good today?"
8. Artists are not allowed to peek at other artist's progress.
9. **Drawing Phase** ends when either the timer goes off, or all the teams are finished drawing.  When the timer goes off, all artists must put their pens down.  When everyone is ready, they move to the **Judging Phase**.

### Judging Phase

1. Each team reveals the client's card and the artist's artwork to everyone.
2. If 4 or more players are playing, points are rewarded to each team to determine the winners.
3. First, every player votes on the artist's artwork that they think most closely resembles the client's card.  Each vote counts as 1 point for that team.
4. Next, the teams that can successfully recall the 3 questions the artist asked the client during the **Drawing Phase**, and the client's respective answers are rewarded additional points: one-half the number of teams in the game, rounded down.
	* Example: if there are 2 teams, the team who fulfilled the 3-questions bonus gets 1 extra point; 3 teams, 1 extra point; and 4 teams, 2 extra points.
5. The team with the most points wins.  If there are ties, the tying teams are all considered winners.

## Gestures

The gesture language that the client must use to communicate to the artists is listed below.  All players should review this section unless they're already informed.

### Direction Gesture

A client provides a direction to draw by simply pointing to a direction perpendicular to the direction they're facing.  This gesture is meant to be used with the other gestures listed below to create a sentence.  Note that the indicated direction is relative to the artist.

The rest of the document will refer to this gesture as, **Direction Gesture.**

#### Examples

![Pointing up](https://bytebucket.org/OmiyaGames/ggj2017/raw/default/gestures/direction-up.png) ![Pointing right](https://bytebucket.org/OmiyaGames/ggj2017/raw/default/gestures/direction-right.png) ![Pointing down-left](https://bytebucket.org/OmiyaGames/ggj2017/raw/default/gestures/down-left.png)

* Pointing up stands for, "draw up"
* Pointing right relative to the artist stands for, "draw right"
* Pointing down-left relative to the artist stands for, "draw down-left"

### Straight Line Gesture

When the client swings their arms up-and-down twice, followed by a **Direction Gesture**, this informs their artist to draw a straight line from their pen's current position to the indicated direction.

#### Example

![Straight Line Upwards](https://bytebucket.org/OmiyaGames/ggj2017/raw/default/gestures/straight-line.png) 

When the client

1. swings their arms up-and-down twice,
2. then points up,
3. that stands for, "draw a straight line upwards."

### Curved Line Gesture

When the client waves their arms into an L-formation, then to a mirrored L-formation twice, this indicates to the artist to start drawing a curved line from their pen's current position (the rest of the document will refer to this as the **Start Curved Line Gesture**).  This gesture is followed by a **Direction Gesture**, then crossing the arms into an X, and finally a second **Direction Gesture** to indicate the starting and ending direction of the curve.

#### Example

![Curved Line Up-right](https://bytebucket.org/OmiyaGames/ggj2017/raw/default/gestures/curve-line.png) 

When the client

1. waves their arms into the **Start Curved Line Gesture**,
2. then points up,
3. crosses their arm into an X,
4. then points right relative to the artist,
5. that stands for, "draw a curved line that starts going upwards, then gradually curves to the right."

### Sharper Curve Gesture

If the client needs to indicate to the artist either

1. a start of a curve that turns more sharply in one direction, or,
2. indicate to the artist that the trajectory of their curved line needs modification,

the client can use 2 arms (rather than the typical 1) to convey the direction the artist needs to curve their line more sharply.

#### Examples

![Curve More Sharply](https://bytebucket.org/OmiyaGames/ggj2017/raw/default/gestures/curve-more.png) 

* When the artist
	1. draws a curve going from right to down, but needs to curve downwards more sharply,
	2. and the client points their 2 arms down,
	3. that stands for, "curve the line more downwards."
* When the client
	1. waves their arms into the **Start Curved Line Gesture**,
	2. then points up with 2 arms,
	3. crosses their arm into an X,
	4. then points right with 1 arm relative to the artist,
	5. that stands for, "draw a curved line that starts going upwards, then barely curves to the right."
* When the client
	1. waves their arms into the **Start Curved Line Gesture**,
	2. then points up with 1 arm,
	3. crosses their arm into an X,
	4. then points right with 2 arms relative to the artist,
	5. that stands for, "draw a curved line that starts going upwards, then sharply curves to the right."

### Stop Gesture

When the client claps, the artist should stop moving their pen and wait for the next instruction.

#### Example

![Clap](https://bytebucket.org/OmiyaGames/ggj2017/raw/default/gestures/stop.png) 

When the artist

1. keeps drawing a straight line to the right,
2. and the client claps,
3. that stands for, "stop drawing the straight line in that direction."

### Keep Going Gesture

If the client needs to indicate to the artist that they need to continue drawing the line they've started, the client can squat up-and-down.  This gesture can be combined with a **Direction Gesture** or **Sharper Curve Gesture** for clarity.

#### Example

![Squat](https://bytebucket.org/OmiyaGames/ggj2017/raw/default/gestures/keep-going.png) 

When the artist

1. stops drawing a straight line to the right,
2. but the client is squatting up-and-down (possibly while still pointing 1 arm to the right),
3. that stands for, "keep drawing that straight line to the right."

## Create A Custom Deck of Diagrams

*Picture This! An Interpretative Art Exercise* can be customized and expanded as the players pleases!  Anyone can create their own deck of diagrams by creating cards with a simple line art that follows these rules:

1. The diagram must be drawable without lifting the pen off the paper.
2. The diagram must not require the artist to trace back a previously-drawn line.  Note that intersecting a previously-drawn line is permitted.

### Recommendations

The list below provides advices for the player's own diagrams that, while not mandatory, improves the experience of this game:

1. Diagrams have at least 1 straight line and 1 curved lines are recommended.
2. If a diagram is composed entirely of straight lines, it is recommended to have at least 1 of those lines be at an angle that isn't a multiple of 45 degrees.
3. Diagrams that needs between 5 to 12 separate instructions are recommended.  To calculate the number of instructions, assume straight lines require 1 instruction, and circles require 4 instructions; 1 for each curve that makes the quarter of a circle.
4. Diagrams that resemble a well-known shape or symbol are recommended.
5. Last but not least, test the diagram with a friend, and see if it's fun to draw.  Playtesting provides the ultimate answer to whether a diagram is successful or not!

## Credits

* Taro Omiya, Game Designer
* Brian Shurtleff, Photographer
